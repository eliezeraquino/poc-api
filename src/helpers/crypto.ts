import { EncryptionTransformer } from 'typeorm-encrypted';

export const hashColumn = new EncryptionTransformer({
  key: `89650F19E6E27B4070B1FB3981DC145189650F19E6E27B4070B1FB3981DC1451`,
  algorithm: 'aes-256-cbc',
  ivLength: 16,
  iv: '5CC69B45D582ACCF2A24CC45D437B16A',
});
