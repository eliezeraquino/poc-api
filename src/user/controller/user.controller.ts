import {
  Body,
  Controller,
  Get,
  HttpCode,
  Post,
  UseGuards,
  Put,
  Param,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../../auth/guard/jwt-auth.guard';
import { CreateUserDto } from '../model/dto/CreateUserDto';
import { LoginUserDto } from '../model/dto/LoginUserDto';
import { UpdateUserDto } from '../model/dto/UpdateUserDto';
import { UserI } from '../model/user.interface';
import { UserService } from '../service/user.service';

@Controller('users')
@ApiTags('Users')
export class UserController {
  constructor(private userService: UserService) {}

  @Post('login')
  @HttpCode(200)
  // eslint-disable-next-line @typescript-eslint/ban-types
  login(@Body() loginUserDto: LoginUserDto): Promise<Object> {
    return this.userService.login(loginUserDto).then((jwt: string) => {
      return {
        access_token: jwt,
        token_type: 'JWT',
        expires_in: 10000,
      };
    });
  }

  @Post()
  add(@Body() createUserDto: CreateUserDto): Promise<UserI> {
    return this.userService.add(createUserDto);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<UserI> {
    return this.userService.update(Number(id), updateUserDto);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get()
  findAll(): Promise<UserI[]> {
    return this.userService.findAll();
  }
}
