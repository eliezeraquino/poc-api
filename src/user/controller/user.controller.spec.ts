import { CreateUserDto } from '../model/dto/CreateUserDto';
import { LoginUserDto } from '../model/dto/LoginUserDto';
import { UserController } from './user.controller';

describe('UserController', () => {
  let controller: UserController;
  const mockUser = new CreateUserDto();
  const mockLogin = new LoginUserDto();

  beforeEach(async () => {
    mockUser.name = 'Teste';
    mockUser.email = 'teste@gmail.com';
    mockUser.password = '123456789';
    mockUser.phone = '15912345678';

    mockLogin.email = 'teste@gmail.com';
    mockLogin.password = '123456789';

    const service = {
      add: async () => Promise.resolve(),
      login: async () => Promise.resolve(),
      save: async () => Promise.resolve(),
      find: async () => Promise.resolve(),
      findAll: async () => Promise.resolve([]),
      findOne: async () => Promise.resolve(),
      update: async () => Promise.resolve(),
    };

    controller = new UserController(service as any);
  });

  it('Create', async () => {
    expect(await controller.add(mockUser)).toBeDefined();
  });

  it('Find All', async () => {
    expect(await controller.findAll()).toBeDefined();
  });

  it('Update', async () => {
    expect(await controller.update('2', mockUser)).toBeDefined();
  });

  it('Login', async () => {
    expect(await controller.login(mockLogin)).toBeDefined();
  });
});
