import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength, MinLength } from 'class-validator';
import { LoginUserDto } from './LoginUserDto';

export class CreateUserDto extends LoginUserDto {
  @ApiProperty({
    type: String,
  })
  @IsString()
  @MinLength(6)
  @MaxLength(60)
  name: string;

  @ApiProperty({
    type: String,
  })
  @IsString()
  @MaxLength(11)
  phone: string;
}
