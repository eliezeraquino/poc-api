import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';

export class LoginUserDto {
  @ApiProperty({
    type: String,
  })
  @IsEmail()
  @MinLength(6)
  email: string;

  @ApiProperty({
    type: String,
  })
  @IsNotEmpty()
  @MinLength(6)
  password: string;
}
