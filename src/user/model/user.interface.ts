export interface UserI {
  id: number;
  name: string;
  phone: string;
  email: string;
  password?: string;
}
