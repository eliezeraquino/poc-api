import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthService } from '../../auth/service/auth.service';
import { Repository } from 'typeorm';
import { CreateUserDto } from '../model/dto/CreateUserDto';
import { UserEntity } from '../model/user.entity';
import { UserI } from '../model/user.interface';
import { LoginUserDto } from '../model/dto/LoginUserDto';
import { UpdateUserDto } from '../model/dto/UpdateUserDto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    private authService: AuthService,
  ) {}

  login(loginUserDto: LoginUserDto): Promise<any> {
    return this.findUserByEmail(loginUserDto.email).then((user: UserI) => {
      if (user) {
        return this.validatePassword(loginUserDto.password, user.password).then(
          (passwordsMatches: boolean) => {
            if (passwordsMatches) {
              return this.findOne(user.id).then((user: UserI) =>
                this.authService.generateJwt(user),
              );
            } else {
              throw new HttpException(
                'Login was not Successfulll',
                HttpStatus.UNAUTHORIZED,
              );
            }
          },
        );
      } else {
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      }
    });
  }

  add(createdUserDto: CreateUserDto): Promise<UserI> {
    return this.mailExists(createdUserDto.email).then((exists: boolean) => {
      if (!exists) {
        return this.authService
          .hashPassword(createdUserDto.password)
          .then((passwordHash: string) => {
            createdUserDto.password = passwordHash;
            return this.userRepository
              .save(createdUserDto)
              .then((savedUser: UserI) => {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                const { password, ...user } = savedUser;
                return user;
              });
          });
      } else {
        throw new HttpException('Email already in use', HttpStatus.CONFLICT);
      }
    });
  }

  findAll(): Promise<UserI[]> {
    return this.userRepository.find();
  }

  findOne(id: number): Promise<UserI> {
    return this.userRepository.findOne({ id });
  }

  private findUserByEmail(email: string): Promise<UserI> {
    return this.userRepository.findOne(
      { email },
      { select: ['id', 'email', 'name', 'password'] },
    );
  }

  private validatePassword(
    password: string,
    storedPasswordHash: string,
  ): Promise<boolean> {
    return this.authService.comparePasswords(password, storedPasswordHash);
  }

  private async mailExists(email: string): Promise<boolean> {
    const user = await this.userRepository.findOne({ email });
    if (user) {
      return true;
    } else {
      return false;
    }
  }

  update(id: number, updateUserDto: UpdateUserDto): Promise<UserI> {
    return this.userRepository.save({ ...updateUserDto, id: id });
  }
}
