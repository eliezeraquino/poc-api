// import { AuthService } from "./../../auth/service/auth.service"
// import { CreateUserDto } from "../model/dto/CreateUserDto"
// import { LoginUserDto } from "../model/dto/LoginUserDto"
// import { UserService } from "./user.service"
// import { Test, TestingModule } from "@nestjs/testing"

// describe('UserService', () => {
//   let service: UserService
//   let authService: AuthService
//   let mockUser = new CreateUserDto()
//   let mockLogin = new LoginUserDto()

//   beforeEach(async () => {
//     const repository = {
//       add: async () => Promise.resolve(),
//       login: async () => Promise.resolve(),
//       save: async () => Promise.resolve(),
//       find: async () => Promise.resolve(),
//       findAll: async () => Promise.resolve([]),
//       findOne:async()=>Promise.resolve(),
//       update:async()=>Promise.resolve(),
//     }
//     authService = new AuthService('123456' as any)
//     service = new UserService(repository as any, authService)

//     const module: TestingModule = await Test.createTestingModule({
//       providers: [
//         UserService,
//         {
//           useFactory: mockUser,
//         },
//       ],
//     }).compile()

//     userRepository = await module.get<UserRepository>(UserRepository)
//     service = await module.get<UsersService>(UsersService)

//     mockUser.name = 'Teste'
//     mockUser.email = 'teste@gmail.com'
//     mockUser.password = '123456789'
//     mockUser.phone = '15912345678'

//     mockLogin.email = 'teste@gmail.com'
//     mockLogin.password = '123456789'

//   })

//   it('Create', async () => {
//     expect(await service.add(mockUser)).toBeDefined()
//   })

//   it('Find All', async ()=>{
//     expect(await service.findAll()).toBeDefined()
//   })

//   it('Find One',async ()=>{
//     expect(await service.findOne(2)).toBeDefined()
//   })

//   it('Update', async ()=>{
//     expect(await service.update(2,mockUser)).toBeDefined()
//   })

//   it('Login', async ()=>{
//     expect(await service.login(mockLogin)).toBeDefined()
//   })
// })

import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { AuthService } from '../../auth/service/auth.service';
import { JwtService } from '@nestjs/jwt';
describe('UserService', () => {
  let service: UserService;
  let authService: AuthService;
  let jwtService: JwtService;
  const mockRepository = {
    add: jest.fn(),
    login: jest.fn(),
    save: jest.fn(),
    find: jest.fn(),
    findAll: jest.fn(),
    findOne: jest.fn(),
    update: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: AuthService,
          useValue: mockRepository,
        },
      ],
    }).compile();
    jwtService = new JwtService({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '10000s' },
    });
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    authService = new AuthService(jwtService);
    service = module.get<UserService>(UserService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
