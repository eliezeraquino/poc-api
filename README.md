
## Descrição

Projeto para PoC de backend usando:
- [x] Docker
- [x] NestJS
- [x] MySQL
- [x] JWT
- [x] Encriptação de dados no banco

## Instalação

```bash
$ npm install
```

## Rodar a aplicação, ele deve subir a API, banco em postgres e pgAdmin

```bash
$ docker-compose up --build
```

## Teste

```bash
# comentar linha 16 e descomentar linha 18 do docker-compose.yml
$ docker-compose up --build
```
